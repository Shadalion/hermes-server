package com.hermes.server.workers.api;

import java.net.Socket;

public interface HermesWorker {
    void run(Socket s);
}
