package com.hermes.server.workers.api;

import java.util.Properties;

public interface HermesWorkerFactory {
    HermesWorker create(Properties props);
}
