package com.hermes.server;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Main {
    public static void main(String[] args) {
        InputStream in = Main.class.getResourceAsStream("/abc.html");
        Properties config = new Properties();
        try {
            config.load(in);
            System.out.println(config.getProperty("name"));
            System.out.println(config.getProperty("version"));

        } catch (IOException e1) {
            System.err.println(e1);
        }
        //new HermesConnectionAcceptor().startListen(new Properties());
    }
}
