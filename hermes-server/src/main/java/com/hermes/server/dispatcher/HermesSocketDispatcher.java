package com.hermes.server.dispatcher;

import java.net.Socket;

public interface HermesSocketDispatcher {
    void dispatch(Socket socket);
}
