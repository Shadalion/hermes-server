package com.hermes.server.dispatcher.io;

import com.hermes.server.dispatcher.HermesSocketDispatcher;
import com.hermes.server.dispose.HermesDisposable;
import com.hermes.server.workers.HermesWorkerThreadFactory;
import com.hermes.server.workers.api.HermesWorker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.Socket;
import java.util.Properties;
import java.util.concurrent.*;

import static com.hermes.server.dispatcher.io.HermesIOSocketDispatcherConfig.maxSocketQueueSize;
import static com.hermes.server.dispatcher.io.HermesIOSocketDispatcherConfig.threadPoolSize;

public class HermesIOSocketDispatcher implements HermesSocketDispatcher, HermesDisposable {
    private static final Logger LOGGER = LoggerFactory.getLogger(HermesIOSocketDispatcher.class);

    private ThreadPoolExecutor executor;
    private HermesWorker worker;

    //TODO - Сделать инициализацию через injector
    public HermesIOSocketDispatcher(Properties properties) {
        int poolSize = threadPoolSize.getOrDefault(properties);
        BlockingQueue<Runnable> queue = new ArrayBlockingQueue<Runnable>(maxSocketQueueSize.getOrDefault(properties));
        this.executor = new ThreadPoolExecutor(poolSize, poolSize, 0L, TimeUnit.MILLISECONDS, queue, HermesWorkerThreadFactory.getInstance());
    }

    @Override
    public void dispatch(Socket socket) {
        try {
            executor.execute(() -> worker.run(socket));
        } catch (RejectedExecutionException e) {
            //TODO handle reject
        }
    }

    @Override
    public void dispose() {
        try {
            this.executor.shutdown();
        } catch (SecurityException e) {
            LOGGER.error("Can't properly shutdown executor", e);
        }
    }
}
