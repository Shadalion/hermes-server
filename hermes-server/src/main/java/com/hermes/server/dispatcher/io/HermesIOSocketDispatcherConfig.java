package com.hermes.server.dispatcher.io;

import com.hermes.server.config.HermesConfigUnit;

final class HermesIOSocketDispatcherConfig {
    private HermesIOSocketDispatcherConfig() {}

    static final HermesConfigUnit<Integer> threadPoolSize = new HermesConfigUnit<>("hermes.dispatcher.io.poolSize", 50, Integer::parseInt);
    static final HermesConfigUnit<Integer> maxSocketQueueSize = new HermesConfigUnit<>("hermes.dispatcher.io.maxSocketQueueSize", 100, Integer::parseInt);
}
