package com.hermes.server.config;

import java.util.Properties;
import java.util.function.Function;

public final class HermesConfigUnit<T> {
    private String prop;
    private T defaultVal;
    private Function<String, T> parser;

    private HermesConfigUnit() {
    }

    public HermesConfigUnit(String prop, T defaultVal, Function<String, T> parser) {
        this.prop = prop;
        this.defaultVal = defaultVal;
        this.parser = parser;
    }

    @SuppressWarnings("unchecked")
    public T getOrDefault(Properties props) {
        String strVal = props.getProperty(prop);

        if (strVal != null) {
            return parser.apply(strVal);
        }

        Object objVal = props.get(prop);

        if (objVal == null) {
            return defaultVal;
        }

        try {
            return (T) objVal;
        } catch (ClassCastException e) {
            return defaultVal;
        }
    }
}
