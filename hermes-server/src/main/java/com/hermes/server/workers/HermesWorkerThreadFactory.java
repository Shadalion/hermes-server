package com.hermes.server.workers;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class HermesWorkerThreadFactory implements ThreadFactory {

    private static volatile HermesWorkerThreadFactory INSTANCE;

    private final AtomicInteger counter = new AtomicInteger(0);

    private HermesWorkerThreadFactory() {}

    public static HermesWorkerThreadFactory getInstance() {
        if(INSTANCE == null) {
            synchronized (HermesWorkerThreadFactory.class) {
                if (INSTANCE == null) {
                    return INSTANCE = new HermesWorkerThreadFactory();
                }
                return INSTANCE;
            }
        } else {
            return INSTANCE;
        }
    }

    @Override
    public Thread newThread(Runnable r) {
        return new Thread(String.format("hermes-worker-thread-%d", counter.getAndIncrement()));
    }
}
