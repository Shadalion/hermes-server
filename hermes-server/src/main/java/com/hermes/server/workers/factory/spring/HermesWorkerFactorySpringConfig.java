package com.hermes.server.workers.factory.spring;

import com.hermes.server.config.HermesConfigUnit;

final class HermesWorkerFactorySpringConfig {
    private HermesWorkerFactorySpringConfig() {}

    static final HermesConfigUnit<Integer> ctxpath
            = new HermesConfigUnit<>("hermes.workerfactory.spring.ctxpath", "", Integer::parseInt);
}
