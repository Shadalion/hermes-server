package com.hermes.server.dispose;

public interface HermesDisposable {
    //Dispose result?
    void dispose();
}
