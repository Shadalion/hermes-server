package com.hermes.server.dispose;

import com.google.common.eventbus.Subscribe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class HermesDisposeManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(HermesDisposeManager.class);

    private List<HermesDisposable> disposableList;

    public HermesDisposeManager(List<HermesDisposable> disposableList) {
        this.disposableList = disposableList;
    }

    @Subscribe
    public void handleDisposeEvent() {
        for(HermesDisposable disposable : disposableList) {
            try {
                disposable.dispose();
            } catch (Exception e) {
                LOGGER.error("Can't properly dispose for {}", disposable.getClass(), e);
            }
        }
    }
}
