package com.hermes.server.exceptions;

public class HermesInitException extends RuntimeException {
    public HermesInitException(Throwable cause) {
        super(cause);
    }
}
