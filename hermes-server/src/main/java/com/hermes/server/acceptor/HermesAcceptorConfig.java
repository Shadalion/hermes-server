package com.hermes.server.acceptor;

import com.hermes.server.config.HermesConfigUnit;

final class HermesAcceptorConfig {
    private HermesAcceptorConfig() {}

    static final HermesConfigUnit<Integer> port = new HermesConfigUnit<>("hermes.acceptor.port", 8080, Integer::parseInt);
    static final HermesConfigUnit<Integer> rcvbuf = new HermesConfigUnit<>("hermes.acceptor.rcvbuf", 65536, Integer::parseInt);
    static final HermesConfigUnit<Integer> acpttimeout = new HermesConfigUnit<>("hermes.acceptor.AcptTimeout", 0, Integer::parseInt);
    static final HermesConfigUnit<Integer> maxacptretries = new HermesConfigUnit<>("hermes.acceptor.maxAcptRetries", 0, Integer::parseInt);
    static final HermesConfigUnit<Long> errAcptDelay = new HermesConfigUnit<>("hermes.acceptor.errAcptDelay", 0L, Long::parseLong);
}
