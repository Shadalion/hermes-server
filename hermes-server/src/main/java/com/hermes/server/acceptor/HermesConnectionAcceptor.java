package com.hermes.server.acceptor;

import com.google.common.eventbus.EventBus;
import com.hermes.server.dispatcher.HermesSocketDispatcher;
import com.hermes.server.dispose.HermesDisposable;
import com.hermes.server.events.HermesDisposeEvent;
import com.hermes.server.exceptions.HermesInitException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;

import static com.hermes.server.acceptor.HermesAcceptorConfig.*;

public class HermesConnectionAcceptor implements HermesDisposable {
    private static final Logger LOGGER = LoggerFactory.getLogger(HermesConnectionAcceptor.class);

    private EventBus eventBus;
    private HermesSocketDispatcher dispatcher;

    private ServerSocket acceptor;

    public HermesConnectionAcceptor(HermesSocketDispatcher dispatcher,
                                    EventBus eventBus) {
        this.dispatcher = dispatcher;
        this.eventBus = eventBus;
    }

    public void startListen(Properties props) {
        try {
            acceptor = new ServerSocket();
            acceptor.setReceiveBufferSize(rcvbuf.getOrDefault(props));
            acceptor.setSoTimeout(acpttimeout.getOrDefault(props));
            acceptor.bind(new InetSocketAddress(port.getOrDefault(props)));
        } catch (IOException e) {
            LOGGER.error("Can't init server socket, reason", e);
            throw new HermesInitException(e);
        }

        int retries = 0;

        while (true) {
            try {
                Socket in = acceptor.accept();
                dispatcher.dispatch(in);
                retries = 0;
            } catch (IOException e) {
                if (acceptor.isClosed()) {
                    LOGGER.error("Can't accept incoming connection, probably because of server socket was closed", e);
                    return;
                }

                LOGGER.error("Can't accept incoming connection", e);

                if (retries++ >= maxacptretries.getOrDefault(props)) {
                    postDisposeEvent();
                    break;
                } else {
                    try {
                        Thread.sleep(errAcptDelay.getOrDefault(props));
                    } catch (InterruptedException ie) {
                        Thread.currentThread().interrupt();
                        postDisposeEvent();
                        break;
                    }
                }
            }
        }
    }

    public void dispose() {
        if (acceptor != null && !acceptor.isClosed()) {
            try {
                acceptor.close();
            } catch (IOException e) {
                LOGGER.error("Can't properly close acceptor socket", e);
            }
        }
    }

    private void postDisposeEvent() {
        try {
            eventBus.post(new HermesDisposeEvent());
        } catch (Exception e) {
            LOGGER.error("Exception while posting dispose event");
        }
    }
}
