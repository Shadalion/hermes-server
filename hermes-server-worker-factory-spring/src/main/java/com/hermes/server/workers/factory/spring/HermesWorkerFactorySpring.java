package com.hermes.server.workers.factory.spring;

import com.hermes.server.workers.api.HermesWorker;
import com.hermes.server.workers.api.HermesWorkerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Properties;

public class HermesWorkerFactorySpring implements HermesWorkerFactory {
    @Override
    public HermesWorker create(Properties props) {
        ClassPathXmlApplicationContext ctx =
                new ClassPathXmlApplicationContext(props.getProperty("hermes.workers.factory.spring.ctxName"));
        return ctx.getBean(HermesWorker.class);
    }
}
